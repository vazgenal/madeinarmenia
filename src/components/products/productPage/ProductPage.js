import React, {useEffect, useState} from "react";
import classes from "./ProductPage.module.css";
import Stars from "../../stars/Stars";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {addOrDeleteCard, getOneProduct} from "../../../actions/productsAction";
import Loader from "../../loader/Loader";

const ProductPage = ({addOrDeleteCard, product, getOneProduct, match: {params: {userId}}}) => {
    const [isLoading, setIsLoading] = useState(true);
    
    useEffect(() => {
        getOneProduct(userId, setIsLoading)
    }, []);
    
    return (
        <>
            {
                isLoading ? <Loader /> :
                    <>
                        <h1>{product.title}</h1>
    
                        <div className={classes.productPage}>
                            <div className={classes.productImages}>
                                {product.images.map((img, index) => {
                                    return <img key={index + 1} className={classes.img} src={`/${img}`} alt=""/>
                                })}
                            </div>
                            <div className={classes.productContent}>
                                <div className={classes.productHeader}>
                                    <div>
                                        <h3>{product.title}</h3>
                    
                                        <Stars rating={product.rating}/>
                                    </div>
                                    <div>
                                        <strong className={classes.price}>{product.price} $</strong>
                                    </div>
                                </div>
                                <button className={classes.btn} onClick={() => addOrDeleteCard(userId)}>
                                    {product.checked ? 'remove' : 'add'}
                                </button>
                            </div>
                        </div>
                    </>
            }
        </>
    )
};

const mapStateToProps = state => ({
    product: state.products.product
});

const mapDispatchToProps = {
    getOneProduct,
    addOrDeleteCard
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ProductPage));