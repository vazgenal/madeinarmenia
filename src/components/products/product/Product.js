import React from "react";
import classes from "./Product.module.css";
import {NavLink} from "react-router-dom";

export const Product = ({img, title, id, addOrDeleteCard, checked}) => {
    return (
        <div className={classes.product}>
            <img className="img" src={`/${img}`} alt={title}/>
            <button className={classes.productAdd} onClick={() => addOrDeleteCard(id)}>
                {checked ? <span className="icon-check1"> </span> : <span className="icon-plus"> </span>}
            </button>
            <NavLink className={classes.productMore} to={`/products/${id}`}>
                <h3>{title}</h3>
            </NavLink>
        </div>
    )
};