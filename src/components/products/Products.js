import React, {useEffect, useState} from "react";
import classes from "./Products.module.css";
import {Product} from "./product/Product";
import {addOrDeleteCard, getProducts} from "../../actions/productsAction";
import {connect} from "react-redux";
import Loader from "../loader/Loader";

let offset = 0;
const count = 6;

const Products = ({productCount, title, showMoreBtn = true, products, addOrDeleteCard, getProducts}) => {
    const [isLoading, setIsLoading] = useState(false);
    
    useEffect(() => {
        if (!products.length) {
            getProducts(offset, count)
        }
    }, []);
    
    let showProducts = [...products];
    if (productCount) {
        showProducts = showProducts.filter((value, index) => index < productCount)
    }
    
    if (products.length < offset + count) {
        showMoreBtn = false
    }
    
    function getMoreProducts() {
        offset = offset + count;
        setIsLoading(true);
        getProducts(offset, count, setIsLoading);
    }
    
    return (
        <>
            {title ? <h2>{title}</h2> : <h1>Products</h1>}
            {
                showProducts.length ?
                    <div>
                        <div className={classes.productsRow}>
                            {
                                showProducts.map(({img, title, id, checked}) => {
                                    return (
                                        <Product img={img} title={title} key={id} id={id} checked={checked}
                                                 addOrDeleteCard={addOrDeleteCard}/>
                                    )
                                })
                            }
                            {
                                isLoading ? <Loader/> : ''
                            }
                        </div>
                        {
                            showMoreBtn ?
                                <button className={classes.moreProduct}
                                        onClick={getMoreProducts}>More <strong>{count}+</strong></button>
                                : ''
                        }
                    </div>
                    : <Loader/>
            }
        </>
    )
};

const mapStateToProps = state => ({
    products: state.products.products
});

const mapDispatchToProps = {
    addOrDeleteCard,
    getProducts
};

export default connect(mapStateToProps, mapDispatchToProps)(Products);