import React from "react";
import classes from "./Socials.module.css";

export const Socials = ({socials}) => {
    let socialsItems = socials && socials.map(elem => {
        return <li key={elem.id}>
            <a className={classes.link} href={elem.url} target="_blank">
                <span className={`icon-${elem.icon}`}></span>
            </a>
        </li>
    });
    
    return (
        <div className={classes.socials}>
            <ul className={`${classes.menu} menu`}>
                {socialsItems}
            </ul>
        </div>
    )
};