import React, {useEffect, useState} from "react";
import classes from "./Header.module.css";
import Logo from "../logo/Logo";
import {Nav} from "./nav/Nav";
import {Socials} from "./socials/Socials";
import {NavLink} from "react-router-dom";
import {connect} from "react-redux";
import {getNav, getShopCard, getSocials} from "../../actions/pageInfoAction";

const Header = ({socials, nav, getNav, getSocials, getShopCard, shopCard}) => {
    const [menuToggle, setMenuToggle] = useState(false);
    
    useEffect(() => {
        getNav();
        getSocials();
        getShopCard();
    }, []);
    
    return (
        <header className={`${classes.miaHeader} ${menuToggle && classes.active}`} id="miaHeader">
            <button className={classes.menuBtn}
                    onClick={() => setMenuToggle(!menuToggle)}>
                <span className="icon-menu"></span>
            </button>
            
            <button className={`${classes.menuToggle} ${menuToggle && classes.active}`}
                    onClick={() => setMenuToggle(!menuToggle)}>
                <span className="icon-left"></span>
            </button>
            
            <Logo/>
            
            <Nav nav={nav}/>
            
            <Socials socials={socials}/>
            
            <div className={classes.loginCart}>
                <NavLink to="/login" className={classes.loginBtn}>
                    <span className={`${classes.iconLogin} icon-login`}></span>
                    <span className={`${classes.iconUser} icon-user`}></span>
                    <div className={classes.userName}>User name</div>
                </NavLink>
                <NavLink to="/carts" className={classes.headerCart}>
                    <span className="icon-cart"></span>
                    <span className={classes.cartCount}>{shopCard.list ? shopCard.list.length : 0}</span>
                </NavLink>
            </div>
        </header>
    )
};

const mapStateToProps = state => ({
    products: state.products.products,
    socials: state.pageInfo.socials,
    nav: state.pageInfo.nav,
    shopCard: state.products.shopCard
});

const mapStateToDispatch = {
    getNav, getSocials, getShopCard
};

export default connect(mapStateToProps, mapStateToDispatch)(Header);