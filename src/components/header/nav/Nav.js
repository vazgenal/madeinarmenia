import classes from "./Nav.module.css";
import React from "react";
import {NavLink} from "react-router-dom";

export const Nav = ({nav}) => {
    let links = nav && nav.map(elem => {
        return <li key={elem.id}>
            <NavLink className={classes.menuLink} activeClassName={classes.active} to={'/' + elem.url}> { elem.text } </NavLink>
        </li>
    });
    
    return (
        <nav className={classes.menuList}>
            <ul className={`${classes.menu} menu`}>
                { links }
            </ul>
        </nav>
    );
};