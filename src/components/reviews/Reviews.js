import React, {useEffect} from "react";
import classes from "./Reviews.module.css";
import Stars from "../stars/Stars";
import {connect} from "react-redux";
import {getReviews} from "../../actions/pageInfoAction";
import Loader from "../loader/Loader";

const Reviews = ({reviews, getReviews}) => {
    useEffect(() => {
        if (!reviews.length) {
            getReviews()
        }
    }, []);
    
    return (
        <>
            <h1>Reviews</h1>
            
            {
                reviews.length ?
                    <div>
                        {reviews.map(review => {
                            let fullName = review.name.trim().split(' ');
                            let n = '', l = '';
                            if (fullName.length === 1) {
                                n = fullName[0].slice(0, 1);
                            } else {
                                n = fullName[0].slice(0, 1);
                                l = fullName[1].slice(0, 1);
                            }
            
                            return (
                                <div className={classes.review} key={review.id}>
                                    <div className={classes.reviewHeader}>
                                        <div className={classes.reviewImg}>
                                            <img className="img" src={review.img} alt={review.title}/>
                                            <strong>{n + '' + l}</strong>
                                        </div>
                                        <div className={classes.reviewName}>
                                            <h3 className="h3">{review.name}</h3>
                                            <Stars rating={review.rating}/>
                                        </div>
                                    </div>
                    
                                    <div className={classes.reviewContent}>
                                        <div className={classes.reviewDate}>{review.date}</div>
                                        <div>
                                            <h5>{review.title}</h5>
                                            <p className="text-muted mb-0">{review.text}</p>
                                        </div>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                    : <Loader/>
            }
        </>
    )
};

const mapStateToProps = state => ({
    reviews: state.pageInfo.reviews
});

const mapStateToDispatch = {
    getReviews
};

export default connect(mapStateToProps, mapStateToDispatch)(Reviews);