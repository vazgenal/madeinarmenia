import React from "react";
import classes from "./Stars.module.css";

const Stars = ({rating}) => {
    return (
        <div className={classes.stars}>
            <div className={classes.star + ' ' + classes.one}>
                <span className="icon-star"></span>
                <span className="icon-star"></span>
                <span className="icon-star"></span>
                <span className="icon-star"></span>
                <span className="icon-star"></span>
            </div>
            <div className={classes.star + ' ' + classes.two} style={{width: rating + '%'}}>
                <span className="icon-star"></span>
                <span className="icon-star"></span>
                <span className="icon-star"></span>
                <span className="icon-star"></span>
                <span className="icon-star"></span>
            </div>
        </div>
    )
};

export default Stars;