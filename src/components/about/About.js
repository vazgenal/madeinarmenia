import React, {useEffect} from "react";
import classes from "./About.module.css";
import {connect} from "react-redux";
import {getHistory, getTeams} from "../../actions/pageInfoAction";
import Loader from "../loader/Loader";
import {Team} from "./team/Team";

const About = (props) => {
    const {getTeams, getHistory} = props;
    const {history, teams} = props.state;
    
    useEffect(() => {
        if (!history.title) {
            getHistory();
        }
        if (!teams.length) {
            getTeams();
        }
    }, []);
    
    return (
        <>
            <h1>About Us</h1>
            {
                history.title ?
                    <div className={classes.aboutContent}>
                        <h3>{history.title}</h3>
                        <div>
                            <p>{history.text1}</p>
                        </div>
                        <div>
                            <p>{history.text2}</p>
                        </div>
                    </div>
                    : <Loader />
            }
            {
                teams.title ?
                    <div className={classes.teamsBlock}>
                        <h3>{teams.title}</h3>
        
                        <div className={classes.teams}>
                            {teams.persons && teams.persons.map(team => {
                                return <Team key={team.id} img={team.img} name={team.name} rank={team.name} about={team.about} />
                            })}
                        </div>
                    </div>
                    : <Loader />
            }
        </>
    )
};

const mapStateToProps = state => ({
    state: state.pageInfo
});

const mapDispatchToProps = {
    getTeams, getHistory
};

export default connect(mapStateToProps, mapDispatchToProps)(About);