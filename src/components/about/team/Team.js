import classes from "./Team.module.css";
import React from "react";

export const Team = ({img, name, rank, about}) => {
    return <div className={classes.team}>
        <div className={classes.img}>
            <img className="img" src={img} alt={name}/>
        </div>
        <div className={classes.about}>
            <h4>{name}</h4>
            <small>{rank}</small>
            <p>{about}</p>
        </div>
    </div>
}