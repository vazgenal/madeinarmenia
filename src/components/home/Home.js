import React, {useEffect} from "react";
import classes from "./Home.module.css";
import Loader from "../loader/Loader";
import {connect} from "react-redux";
import Products from "../products/Products";
import {getDiscount} from "../../actions/pageInfoAction";
import {NavLink} from "react-router-dom";

const Home = props => {
    const {getDiscount} = props;
    const {title, text, img} = props.pageInfo;
    
    useEffect(() => {
        if(!title) {
            getDiscount()
        }
    }, []);
    
    return (
        <>
            {
                title ?
                    <React.Fragment>
                        <h1>Home</h1>
                        <div className={classes.homeSlider}>
                            <div className={classes.homeSlide}>
                                <div className={classes.slideContent}>
                                    <h3>{title}</h3>
                                    <p>{text}</p>
                                    <NavLink to="" className={classes.slideMore}>More</NavLink>
                                </div>
                                <div className={classes.slideImg}>
                                    <img className="img" src={`/${img}`} alt="" />
                                </div>
                            </div>
                        </div>
                    </React.Fragment>
                    : <Loader />
            }
            
            <Products productCount={3} showMoreBtn={false} title={'Our bestsellers'} />
        </>
    )
};

let mapStateToProps = state => ({
    pageInfo: state.pageInfo.discount
});

const mapStateToDispatch = {
    getDiscount
};

export default connect(mapStateToProps, mapStateToDispatch)(Home);