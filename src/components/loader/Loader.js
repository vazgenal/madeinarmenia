import React from "react";
import classes from "./Loader.module.css";

const Loader = () => {
    return (
        <div className={classes.preloader}>
            <div className={classes.spinner}>
                <span className={classes.item1 + ' ' + classes.item}></span>
                <span className={classes.item2 + ' ' + classes.item}></span>
                <span className={classes.item3 + ' ' + classes.item}></span>
                <span className={classes.item4 + ' ' + classes.item}></span>
            </div>
        </div>
    )
};

export default Loader;