import React, {useState} from "react";
import classes from "./Login.module.css";

const Login = () => {
    const [logReg, setLogReg] = useState(true);
    
    return (
        <React.Fragment>
            <h1>Login / Registration</h1>
            
            <div className={classes.loginBlock}>
                <div className={classes.loginTop}>
                    <button className={`${classes.loginSubmit} ${logReg && classes.active}`}
                            onClick={() => setLogReg(true)}>
                        Login
                    </button>
                    <button className={`${classes.loginSubmit} ${!logReg && classes.active}`}
                            onClick={() => setLogReg(false)}>
                        Registration
                    </button>
                </div>
                {
                    logReg ?
                        <form action="" name="login">
                            <label className={classes.loginItem}>
                                <p>Login</p>
                                <input type="text" placeholder="login or phone"/>
                            </label>
                            <label className={classes.loginItem}>
                                <p>Password</p>
                                <input type="password" placeholder="password"/>
                            </label>
                            <button className={classes.loginSubmit} type="submit">Login</button>
                        </form>
                        :
                        <form action="" name="registration">
                            <label className={classes.loginItem}>
                                <p>Login</p>
                                <input type="text" placeholder="login or phone"/>
                            </label>
                            <label className={classes.loginItem}>
                                <p>Password</p>
                                <input type="text" placeholder="password"/>
                            </label>
                            <label className={classes.loginItem}>
                                <p>Repeat Password</p>
                                <input type="text" placeholder="password"/>
                            </label>
                            <button className={classes.loginSubmit} type="submit">Registration</button>
                        </form>
                }
            
            </div>
        </React.Fragment>
    )
};

export default Login;