import classes from "./Cart.module.css";
import {NavLink} from "react-router-dom";
import React from "react";

export const Cart = ({cart, incrementDecrement, deleteCard}) => {
    const incrementDecrementHandler = (text) => {
        if(text === 'minus') {
            if (cart.quantity > 1) {
                incrementDecrement(cart.id, 'minus')
            }
        } else {
            incrementDecrement(cart.id, 'plus')
        }
    };
    
    const deleteItem = () => {
        deleteCard(cart.id)
    };
    
    return (
        <>
            <div className={classes.cart}>
                <div className={classes.cartImg}>
                    <img className="img" src={cart.img} alt={cart.title}/>
                </div>
                
                <h4 className={classes.cartTitle}><NavLink to={`products/${cart.id}`}>{cart.title}</NavLink></h4>
                
                <strong className={classes.cartPrice}>$ {cart.price * cart.quantity}</strong>
                
                <div className={classes.cartCounter}>
                    <button className={classes.countBtn + ' ' + classes.minus} onClick={() => incrementDecrementHandler('minus')}>
                        <span className="icon-minus"> </span></button>
                    <span className={classes.count}>{cart.quantity}</span>
                    <button className={classes.countBtn + ' ' + classes.plus} onClick={() => incrementDecrementHandler('plus')}>
                        <span className="icon-plus"> </span></button>
                </div>
                
                <button className={classes.remove} onClick={deleteItem}><span className="icon-close"> </span></button>
            </div>
        </>
    )
};