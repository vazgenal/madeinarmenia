import React, {useEffect} from "react";
import classes from "./Carts.module.css";
import {connect} from "react-redux";
import {addOrDeleteCard, incrementDecrement} from "../../actions/productsAction";
import {getShopCard} from "../../actions/pageInfoAction";
import {Cart} from "./cart/Cart";

const Carts = ({incrementDecrement, addOrDeleteCard, shopCard, getShopCard}) => {
    useEffect(() => {
        if (!shopCard.list.length) {
            getShopCard();
        }
    }, []);
    
    return (
        <>
            <h1>Shopping cart</h1>
            
            <div className={classes.carts}>
                {shopCard.list.map(cart => {
                    return (
                        <Cart cart={cart} key={cart.id} incrementDecrement={incrementDecrement} deleteCard={addOrDeleteCard} />
                    )
                })}
            </div>
            
            <div className={classes.total}>
                <span>Total \ </span>
                <strong>{shopCard.price}</strong>
                <strong>$</strong>
            </div>
        </>
    )
};

const mapStateToProps = state => ({
    products: state.products.products,
    shopCard: state.products.shopCard
});

const mapDispatchToProps = {
    addOrDeleteCard,
    getShopCard,
    incrementDecrement
};

export default connect(mapStateToProps, mapDispatchToProps)(Carts);