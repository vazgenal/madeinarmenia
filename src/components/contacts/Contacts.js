import React, {useEffect} from "react";
import classes from "./Contacts.module.css";
import {connect} from "react-redux";
import {getContacts} from "../../actions/pageInfoAction";
import Loader from "../loader/Loader";

const Contacts = ({contacts, getContacts}) => {
    const {address1, address2, phones} = contacts;
    
    useEffect(() => {
        if (!phones) {
            getContacts()
        }
    }, []);
    
    return (
        <>
            <h1>Contacts</h1>
            {
                phones ?
                    <div className={classes.contactBlock}>
                        <div className={classes.contacts}>
                            <h4>Address</h4>
                            <p>{address1}</p>
                            <p>{address2}</p>
                            <hr/>
                            <h4>Phone numbers</h4>
            
                            {phones && phones.map(item => {
                                return (
                                    <p key={item.id}>
                                        <strong>{item.name}</strong><br/>
                                        <a href={`tel:${item.phone}`}>{item.phone}</a>
                                    </p>
                                )
                            })}
        
                        </div>
                    </div>
                    : <Loader />
            }
        </>
    )
};

const mapStateToProps = state => ({
    contacts: state.pageInfo.contacts
});

const mapStateToDispatch = {
    getContacts
};

export default connect(mapStateToProps, mapStateToDispatch)(Contacts);