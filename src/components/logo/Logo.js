import classes from "./Logo.module.css";
import React from "react";
import {NavLink} from "react-router-dom";

const Logo = () => {
    return (
        <div className={classes.logoBlock}>
            <NavLink to="/home" className={classes.logo}>
                <svg version="1.1" width="120" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                     viewBox="0 0 156 95">
                    <text transform="matrix(1 0 0 1 14 73)">
                        <tspan x="0" y="0" className={`${classes.st0} ${classes.st1}`}>M</tspan>
                        <tspan x="57.9" y="0" className={`${classes.st0} ${classes.st2}`}>in</tspan>
                        <tspan x="86.3" y="0" className={`${classes.st0} ${classes.st1}`}>A</tspan>
                    </text>
                </svg>
            </NavLink>
        </div>
    )
};

export default Logo;