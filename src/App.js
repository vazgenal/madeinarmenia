import React from 'react';
import './App.css';
import {BrowserRouter, Redirect, Route, Switch} from "react-router-dom";
import {Provider} from "react-redux";
import store from "./reducers/reduxStore";
import Login from "./components/login/Login";
import HomeContainer from "./components/home/Home";
import ProductsPage from "./components/products/productPage/ProductPage";
import Products from "./components/products/Products";
import CartContainer from "./components/carts/Carts";
import HeaderContainer from "./components/header/Header";
import AboutContainer from "./components/about/About";
import ReviewsContainer from "./components/reviews/Reviews";
import ContactsContainer from "./components/contacts/Contacts";

function App() {
    return (
        <BrowserRouter>
            <Provider store={store}>
                <div className="App">
                    <HeaderContainer />
                    <main className="miaMain">
                        <Switch>
                            <Route exact path="/" render={() => <Redirect to={"/home"} />} />
                            <Route path="/home" render={() => <HomeContainer />} />
                            <Route exact path="/products" render={() => <Products />} />
                            <Route path="/products/:userId" render={() => <ProductsPage />} />
                            <Route path="/carts" render={() => <CartContainer />} />
                            <Route path="/about" render={() => <AboutContainer />} />
                            <Route path="/reviews" render={() => <ReviewsContainer />} />
                            <Route path="/contacts" render={() => <ContactsContainer />} />
                            <Route path="/login" component={Login}/>
                            <Route path="*" render={() => <div>404 not found</div>}/>
                        </Switch>
                    </main>
                </div>
            </Provider>
        </BrowserRouter>
    )
}

export default App;
