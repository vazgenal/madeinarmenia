import {getDataProducts, updateShopCard, getOneProductData, updateProductsData} from "../reducers/Store";

export const ADD_OR_DELETE_CARD = "ADD_OR_DELETE_CARD";
export const GET_PRODUCTS = "GET_PRODUCTS";
export const INCREMENT_DECREMENT_CART = "INCREMENT_DECREMENT_CART";
export const PRODUCT_PAGE = "PRODUCT_PAGE";

export const getProducts = (offset, count, callback) => dispatch => {
    getDataProducts(offset, count).then(res => {
        dispatch({type: GET_PRODUCTS, payload: res});
        callback && callback(false);
    });
};

export const getOneProduct = (id, callback) => dispatch => {
    getOneProductData(id).then(res => {
        dispatch({type: PRODUCT_PAGE, payload: res});
        callback(false)
    })
};

export const addOrDeleteCard = id => ({
    type: ADD_OR_DELETE_CARD,
    payload: id
});

export const incrementDecrement = (id, text) => ({
    type: INCREMENT_DECREMENT_CART,
    payload: {id, text}
});

export const updateShopCardData = (data, ms) => {
    let timeout;
    (function () {
        const fnCall = () => updateShopCard(data);
        clearTimeout(timeout);
        timeout = setTimeout(fnCall, ms)
    })()
};

export const updateProducts = (data, ms) => {
    let timeout;
    (function () {
        const fnCall = () => updateProductsData(data);
        clearTimeout(timeout);
        timeout = setTimeout(fnCall, ms)
    })()
};