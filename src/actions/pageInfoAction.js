import {
    getTeamsData,
    getContactsData,
    getReviewsData,
    getHistoryData,
    getDiscountData, getDataNav, getDataSocials, getDataShopCard,
} from "../reducers/Store";

export const PAGE_INFO_TEAMS = 'PAGE_INFO_TEAMS';
export const PAGE_INFO_HISTORY = 'PAGE_INFO_HISTORY';
export const PAGE_INFO_REVIEWS = 'PAGE_INFO_REVIEWS';
export const PAGE_INFO_CONTACTS = 'PAGE_INFO_CONTACTS';
export const PAGE_INFO_DISCOUNT = 'PAGE_INFO_DISCOUNT';
export const PAGE_INFO_NAV = 'PAGE_INFO_NAV';
export const PAGE_INFO_SOCIALS = 'PAGE_INFO_SOCIALS';
export const PAGE_INFO_SHOPCARD = 'PAGE_INFO_SHOPCARD';

export const getNav = () => dispatch => {
    getDataNav().then(res => {
        dispatch({type: PAGE_INFO_NAV, payload: res});
    });
};

export const getSocials = () => dispatch => {
    getDataSocials().then(res => {
        dispatch({type: PAGE_INFO_SOCIALS, payload: res});
    });
};

export const getDiscount = () => dispatch => {
    getDiscountData().then(res => {
        dispatch({type: PAGE_INFO_DISCOUNT, payload: res});
    });
};

export const getTeams = () => dispatch => {
    getTeamsData().then(res => {
        dispatch({type: PAGE_INFO_TEAMS, payload: res});
    });
};

export const getHistory = () => dispatch => {
    getHistoryData().then(res => {
        dispatch({type: PAGE_INFO_HISTORY, payload: res});
    });
};

export const getReviews = () => dispatch => {
    getReviewsData().then(res => {
        dispatch({type: PAGE_INFO_REVIEWS, payload: res});
    });
};

export const getContacts = () => dispatch => {
    getContactsData().then(res => {
        dispatch({type: PAGE_INFO_CONTACTS, payload: res});
    });
};

export const getShopCard = () => dispatch => {
    getDataShopCard().then(res => {
        dispatch({type: PAGE_INFO_SHOPCARD, payload: res});
    });
};