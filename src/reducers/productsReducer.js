import {
    ADD_OR_DELETE_CARD,
    GET_PRODUCTS,
    updateShopCardData,
    INCREMENT_DECREMENT_CART, PRODUCT_PAGE, updateProducts,
} from "../actions/productsAction";
import {PAGE_INFO_SHOPCARD} from "../actions/pageInfoAction";

let initialState = {
    products: [],
    product: {},
    shopCard: {
        list: [],
        price: 0
    }
};

const productsReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_PRODUCTS:
            state.products.splice(state.products.length, 0, ...action.payload);
            return {
                ...state,
                products: [...state.products]
            };
            
        case PRODUCT_PAGE:
            return {
                ...state,
                product: {...action.payload}
            };
            
        case PAGE_INFO_SHOPCARD:
            return {
                ...state,
                shopCard: {...action.payload}
            };
            
        case ADD_OR_DELETE_CARD:
            const newProducts = [...state.products];
            let updateProduct = {};
            let list = [...state.shopCard.list];
            let price = state.shopCard.price;
            newProducts.forEach(value => {
                if (value.id === action.payload) {
                    value.checked = !value.checked;
                    updateProduct = {...value};
                    if(value.checked) {
                        price += value.price;
                        list.push({...value});
                    } else {
                        list.forEach((item, index) => {
                            if(item.id === action.payload) {
                                list.splice(index, 1);
                                price -= item.price * item.quantity;
                            }
                        });
                    }
                }
            });
            updateProducts(updateProduct, 1000);
            updateShopCardData({price: price, list: list}, 1000);
            return {
                ...state,
                products: [...newProducts],
                shopCard: {
                    price: price,
                    list: [...list]
                }
            };
            
        case INCREMENT_DECREMENT_CART:
            let listInc = [...state.shopCard.list];
            let priceInc = state.shopCard.price;
            let newCard = {
                list: [],
                price: 0
            };
            listInc.forEach(elem => {
                if (elem.id === action.payload.id) {
                    if(action.payload.text === 'plus') {
                        elem.quantity += 1;
                        priceInc += elem.price;
                    } else {
                        elem.quantity -= 1;
                        priceInc -= elem.price;
                    }
                }
            });
            newCard.list = listInc;
            newCard.price = priceInc;
            updateShopCardData(newCard, 1000);
            return {
                ...state,
                shopCard: {...newCard}
            };
        
        default:
            return state
    }
};

export default productsReducer;