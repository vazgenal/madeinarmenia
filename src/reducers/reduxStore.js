import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunkMiddleware from "redux-thunk";
import productsReducer from "./productsReducer";
import pageInfoReducer from "./pageInfoReducer";

let reducers = combineReducers({
    products: productsReducer,
    pageInfo: pageInfoReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducers, composeEnhancers(applyMiddleware(thunkMiddleware)));

export default store;