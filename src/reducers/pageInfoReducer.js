import {
    PAGE_INFO_CONTACTS,
    PAGE_INFO_HISTORY,
    PAGE_INFO_REVIEWS,
    PAGE_INFO_TEAMS,
    PAGE_INFO_DISCOUNT, PAGE_INFO_NAV, PAGE_INFO_SOCIALS
} from "../actions/pageInfoAction";

let initialState = {
    history: {},
    teams: {},
    contacts: {},
    reviews: [],
    discount: {},
    nav: [],
    socials: []
};

const pageInfoReducer = (state = initialState, action) => {
    switch (action.type) {
        case PAGE_INFO_NAV:
            return {
                ...state,
                nav: [...action.payload]
            };
        case PAGE_INFO_SOCIALS:
            return {
                ...state,
                socials: [...action.payload]
            };
        case PAGE_INFO_DISCOUNT:
            return {
                ...state,
                discount: {...action.payload}
            };
        case PAGE_INFO_TEAMS:
            return {
                ...state,
                teams: {...action.payload}
            };
        case PAGE_INFO_HISTORY:
            return {
                ...state,
                history: {...action.payload}
            };
        case PAGE_INFO_CONTACTS:
            return {
                ...state,
                contacts: {...action.payload}
            };
        case PAGE_INFO_REVIEWS:
            return {
                ...state,
                reviews: [...action.payload]
            };
        default:
            return state;
    }
};

export default pageInfoReducer;