const store = {
    history: {
        title: 'Our history',
        text1: 'At Apptopia, we all come to work every day because we want to solve the biggest problem in mobile. Everyone is guessing. Publishers don’t know what apps to build, how to monetize them, or even what to price them at. Advertisers & brands don’t know where their target users are, how to reach them, or even how much they need to spend in order to do so. Investors aren’t sure which apps and genres are growing the quickest, and where users are really spending their time (and money).',
        text2: 'At Apptopia, we all come to work every day because we want to solve the biggest problem in mobile. Everyone is guessing. Publishers don’t know what apps to build, how to monetize them, or even what to price them at. Advertisers & brands don’t know where their target users are, how to reach them, or even how much they need to spend in order to do so. Investors aren’t sure which apps and genres are growing the quickest, and where users are really spending their time (and money).'
    },
    discount: {
        title: 'We give a discount new 15%',
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, asperiores!',
        img: 'images/joker.jpg'
    },
    teams: {
        title: 'The MinA Team',
        persons: [
            {
                id: 'teams1',
                img: 'https://st2.depositphotos.com/1291951/6339/i/450/depositphotos_63394993-stock-photo-rebellious-teenager-in-a-hoodie.jpg',
                name: 'Katie Farrell',
                rank: 'AUTHOR & FOUNDER',
                about: 'Blanditiis corporis dignissimos esse exporro quis reiciendis repellat reprehenderit rerum saepe totam unde veniam vero voluptate voluptatum?'
            },
            {
                id: 'teams2',
                img: 'https://st2.depositphotos.com/3616689/9975/i/450/depositphotos_99752186-stock-photo-sad-girl-illustration.jpg',
                name: 'Katie Farrell',
                rank: 'AUTHOR & FOUNDER',
                about: 'Blanditiis corporis dignissimos esse exporro quis reiciendis repellat reprehenderit rerum saepe totam unde veniam vero voluptate voluptatum?'
            },
            {
                id: 'teams3',
                img: 'https://avatarko.ru/img/kartinka/19/devushka_brunetka_ruki_18679.jpg',
                name: 'Katie Farrell',
                rank: 'AUTHOR & FOUNDER',
                about: 'Blanditiis corporis dignissimos esse exporro quis reiciendis repellat reprehenderit rerum saepe totam unde veniam vero voluptate voluptatum?'
            }
        ]
    },
    nav: [
        {id: 'nav1', url: "home", text: "home"},
        {id: 'nav2', url: "products", text: "products"},
        {id: 'nav3', url: "carts", text: "carts"},
        {id: 'nav4', url: "about", text: "about"},
        {id: 'nav5', url: "reviews", text: "reviews"},
        {id: 'nav6', url: "contacts", text: "contacts"}
    ],
    socials: [
        {id: 'socials1', url: "https://facebook.com", icon: "facebook"},
        {id: 'socials2', url: "https://telegram.org/", icon: "telegram"},
        {id: 'socials3', url: "https://www.linkedin.com/", icon: "linkedin"},
        {id: 'socials4', url: "https://www.instagram.com/", icon: "instagram"},
        {id: 'socials5', url: "https://twitter.com/", icon: "twitter"}
    ],
    products: [
        {
            id: 'products1',
            img: 'images/joker.jpg',
            images: ['images/joker.jpg', 'images/joker.jpg', 'images/joker.jpg'],
            title: 'Product name 1',
            price: 20,
            quantity: 1,
            rating: '80',
            checked: false
        },
        {
            id: 'products2',
            img: 'images/joker.jpg',
            images: ['images/joker.jpg', 'images/joker.jpg', 'images/joker.jpg'],
            title: 'Product name 2',
            price: 30,
            quantity: 1,
            rating: '87',
            checked: false
        },
        {
            id: 'products3',
            img: 'images/joker.jpg',
            images: ['images/joker.jpg', 'images/joker.jpg', 'images/joker.jpg'],
            title: 'Product name 3',
            price: 40,
            quantity: 1,
            rating: '90',
            checked: false
        },
        {
            id: 'products4',
            img: 'images/joker.jpg',
            images: ['images/joker.jpg', 'images/joker.jpg', 'images/joker.jpg'],
            title: 'Product name 4',
            price: 50,
            quantity: 1,
            rating: '98',
            checked: false
        },
        {
            id: 'products5',
            img: 'images/joker.jpg',
            images: ['images/joker.jpg', 'images/joker.jpg', 'images/joker.jpg'],
            title: 'Product name 5',
            price: 20,
            quantity: 1,
            rating: '80',
            checked: false
        },
        {
            id: 'products6',
            img: 'images/joker.jpg',
            images: ['images/joker.jpg', 'images/joker.jpg', 'images/joker.jpg'],
            title: 'Product name 6',
            price: 30,
            quantity: 1,
            rating: '87',
            checked: false
        },
        {
            id: 'products7',
            img: 'images/joker.jpg',
            images: ['images/joker.jpg', 'images/joker.jpg', 'images/joker.jpg'],
            title: 'Product name 7',
            price: 40,
            quantity: 1,
            rating: '90',
            checked: false
        },
        {
            id: 'products8',
            img: 'images/joker.jpg',
            images: ['images/joker.jpg', 'images/joker.jpg', 'images/joker.jpg'],
            title: 'Product name 8',
            price: 50,
            quantity: 1,
            rating: '98',
            checked: false
        },
        {
            id: 'products9',
            img: 'images/joker.jpg',
            images: ['images/joker.jpg', 'images/joker.jpg', 'images/joker.jpg'],
            title: 'Product name 9',
            price: 30,
            quantity: 1,
            rating: '87',
            checked: false
        },
        {
            id: 'products10',
            img: 'images/joker.jpg',
            images: ['images/joker.jpg', 'images/joker.jpg', 'images/joker.jpg'],
            title: 'Product name 10',
            price: 40,
            quantity: 1,
            rating: '90',
            checked: false
        },
        {
            id: 'products11',
            img: 'images/joker.jpg',
            images: ['images/joker.jpg', 'images/joker.jpg', 'images/joker.jpg'],
            title: 'Product name 11',
            price: 50,
            quantity: 1,
            rating: '98',
            checked: false
        }
    ],
    reviews: [
        {
            id: 'reviews1',
            img: 'images/joker.jpg',
            name: 'Alice Holy',
            rating: 80,
            date: 'AUGUST 27, 2019',
            title: 'I knew my daughter in law would love it.',
            text: 'When I saw this, I knew my daughter in law would love it, and she did ! We love our fur babies and they are our children. So therefore..... They are his siblings! Thank you for a cute product. I will be back to purchase more items. Thank you, Sandy'
        },
        {
            id: 'reviews2',
            img: 'images/joker.jpg',
            name: 'Holy Alice',
            rating: 72,
            date: 'AUGUST 27, 2019',
            title: 'I knew my daughter in',
            text: 'When I saw this, They are his siblings! Thank you for a cute product. I will be back to purchase more items. Thank you, Sandy'
        },
        {
            id: 'reviews3',
            img: 'images/joker.jpg',
            name: 'Alice Holy',
            rating: 90,
            date: 'AUGUST 27, 2019',
            title: 'I knew my love it.',
            text: 'They are his siblings! Thank you for a cute product. I will be back to purchase more items. Thank you, Sandy'
        }
    ],
    contacts: {
        phones: [
            {id: 'phones1', name: 'Валентин', phone: '+7 (913)-911-87-56'},
            {id: 'phones2', name: 'Марина', phone: '+7 (913)-911-87-56'},
        ],
        address1: 'Новосибирск, ул. Есенина 3/1 складской комплекс «Керамзит»',
        address2: 'Въезд на территорию между Есенина 1а и Есенина 1Б, строение 5, склад 11.',
    },
    shopCard: {
        price: 0,
        list: []
    },
};

function getData() {
    const data = JSON.parse(localStorage.getItem('store'));
    if(data) {
        return data;
    } else {
        addData(store);
        return store
    }
}
function addData(data) {
    localStorage.setItem('store', JSON.stringify(data));
}

//UpdateShopCard
export function updateShopCard(data) {
    let store = getData();
    store.shopCard = data;
    addData(store)
}

//UpdateProduct
export function updateProductsData(data) {
    let store = getData();
    const p = [...store.products];
    p.forEach((value, index) => {
        if(value.id === data.id) {
            p[index] = {...data}
        }
    });
    store.products = [...p];
    addData(store)
}

//ShopCard
export const getDataShopCard = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(getData().shopCard);
        }, 100)
    })
};

//Nav
export const getDataNav = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(getData().nav);
        }, 100)
    })
};

//Socials
export const getDataSocials = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(getData().socials);
        }, 150)
    })
};

//Products
export const getDataProducts = (offset, pageCount) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const allProducts = getData().products;
            let products = [];
            const c = pageCount + offset;
            for (let i = offset; i < c; i++) {
                if(i < allProducts.length){
                    products.push(allProducts[i])
                }
            }
            resolve(products);
        }, 400)
    })
};

//Product
export const getOneProductData = (id) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const allProducts = getData().products;
            let product = allProducts.filter(value => value.id === id);
            resolve(product[0]);
        }, 400)
    })
};

//Discount
export const getDiscountData = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(getData().discount);
        }, 400)
    })
};

//Teams
export const getTeamsData = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(getData().teams);
        }, 900)
    })
};

//History
export const getHistoryData = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(getData().history);
        }, 400)
    })
};

//Reviews
export const getReviewsData = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(getData().reviews);
        }, 400)
    })
};

//Contacts
export const getContactsData = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(getData().contacts);
        }, 400)
    })
};
